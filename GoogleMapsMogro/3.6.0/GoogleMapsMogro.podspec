#
#  Be sure to run `pod spec lint GoogleMapsMogro.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "GoogleMapsMogro"
  spec.version      = "3.6.0"
  spec.summary      = "Google Maps SDK for iOS."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = <<-DESC
  Use the Google Maps SDK for iOS to enrich your app with interactive maps, immersive Street View panoramas, and detailed information from Google's Places database.
                   DESC

  spec.homepage     = "https://developers.google.com/maps/documentation/ios/"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = { :type => "Copyright", :text => "Copyright 2019 Google" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = "Google, Inc."
  # Or just: spec.author    = "Marcelo Mogrovejo"
  # spec.authors            = { "Marcelo Mogrovejo" => "marcelomogrovejo@gmail.com" }
  # spec.social_media_url   = "https://twitter.com/Marcelo Mogrovejo"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # spec.platform     = :ios
  spec.platform     = :ios, "11.0"

  #  When using multiple platforms
  # spec.ios.deployment_target = "5.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  # spec.source       = { :git => "http://EXAMPLE/GoogleMapsMogro.git", :tag => "#{spec.version}" }
  spec.source         = { :http => "https://dl.google.com/dl/cpdc/f5a3789e77faee18/GoogleMaps-3.6.0.tar.gz" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  spec.source_files  = "Classes", "Classes/**/*.{h,m}"
  spec.exclude_files = "Classes/Exclude"

  # spec.public_header_files = "Classes/**/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  spec.preserve_paths = "Example"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"

  # ――― Subspecs Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.default_subspec = 'Maps'

  spec.subspec 'Maps' do |maps|
    maps.dependency 'Base'
    maps.resource = 'Maps/Frameworks/GoogleMaps.framework/Resources/GoogleMaps.bundle'
    maps.vendored_framework = 'Maps/Frameworks/GoogleMaps.framework'
    maps.vendored_framework = 'Maps/Frameworks/GoogleMapsCore.framework'
    maps.frameworks = 'Accelerate', 'CoreImage', 'CoreTelephony', 'CoreText', 'GLKit', 'ImageIO', 'OpenGLES', 'QuartzCore'
  end

  spec.subspec 'Base' do |base|
    base.frameworks = 'CoreData', 'CoreGraphics', 'CoreLocation', 'QuartzCore', 'SystemConfiguration', 'UIKit'
    base.libraries = 'c++', 'z'
    base.vendored_framework = 'Base/Frameworks/GoogleMapsBase.framework'
  end

  spec.subspec 'M4B' do |m4b|
    m4b.dependency 'Maps'
    m4b.vendored_framework = 'M4B/Frameworks/GoogleMapsM4B.framework'
  end

end
